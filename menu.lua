local polywell = require("polywell")
local card = require("card")
local lume = require("lib.lume")
local card_edit = require("modes.card_edit")

local mode = function(m) return lume.fn(polywell.activate_mode, m) end
local edit_card = function() card_edit(card.current()) end
local create_card = function(name, no) if(not no) then card.create(name) end end

-- a table of rings; each ring contains a menu target.
return {
   -- level 1:
   {  {color={255,100,0}, callback = mode("play"),
       image_file="assets/hand.png",}
   },
   -- level 2:
   {  {color={5,200,120}, callback=mode("buttons"),
       image_file="assets/button.png",},
      {color={200,5,120}, callback=edit_card,
       image_file="assets/code.png",},
      {color={20,100,255}, callback=card.jump_select,
       image_file="assets/stack.png",},
      {color={243,255,115}, callback = lume.fn(polywell.read_line,
                                               "New card: ", create_card,
                                               {moused=true}),
       image_file="assets/plus.png",},
      {color={250,5,0}, callback=love.event.quit,
       image_file="assets/quit.png",},
      {color={245,255,46}, callback=mode("character"),
       image_file="assets/char_tool.png"}
   },
   -- level 3: drawing tools
   {
      -- {color={100,200,200}, callback = mode("rect"),
      --  image_file="assets/rect.png",},
      -- {color={200,200,100}, callback = mode("circle"),
      --  image_file="assets/circle.png",},
      {color={250,200,200}, callback = mode("pencil"),
       image_file="assets/pencil.png",},
      {color={200,250,200}, callback = mode("fill"),
       image_file="assets/fill.png",},
      {color={220,100,20}, callback = mode("line"),
       image_file="assets/line.png",},
      {color={200,100,200}, callback = mode("color"),
       image_file="assets/colors.png",},
      {color={20,200,200}, callback = mode("dropper"),
       image_file="assets/dropper.png",}
   },
}
