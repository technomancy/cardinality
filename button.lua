local card = require("card")
local lume = require("lib.lume")

local em = love.graphics.getFont():getWidth('a')
local fh = love.graphics.getFont():getHeight()

return {
   new = function(label, x, y, w, h, code, meta)
      return {label=label, code=code or "local button, card = ...",
              meta=meta or {}, x=x,y=y, w=w or 100,h=h or 20, type="button"}
   end,

   draw = function(btn, show_all)
      if(btn.label == "" and not show_all) then return end
      if(btn.draw) then
         btn:draw()
      else
         love.graphics.setColor(btn.color or btn.bg or {255,255,255})
         love.graphics.rectangle(btn.bg and "fill" or "line",
                                 btn.x, btn.y, btn.w, btn.h)
         love.graphics.setColor({255,255,255})
         love.graphics.print(btn.label, btn.x, btn.y, nil, nil, nil,
                             (#btn.label * em) / 2 - btn.w/2, fh / 2 - btn.h/2)
      end
   end,

   find = function(c, x, y, override)
      for _,button in lume.ripairs(c.buttons or {}) do
         if(x >= button.x and math.floor(x) <= button.x + button.w and
            y >= button.y and math.floor(y) <= button.y + button.h) then
            return button
         elseif(button.meta.modal and not override) then
            return nil
         end
      end
   end,

   activate = function(button)
      if(button.meta.jump_to) then
         card.jump_to(button.meta.jump_to)
      end
      if(button.code) then
         require("execute").load(button, button, card.current())
      end
   end,
}
