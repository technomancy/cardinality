local lume = require("lib.lume")
local card = require("card")
local fields = {"name", "buttons", "image_file", "code", "type", "meta"}
local button_fields = {"label", "code", "meta", "x", "y", "w", "h", "type",
                       "bg", "color"}
local character_fields = {"name", "code", "meta", "x", "y", "w", "h",
                          "type", "costume"}

-- Heads up! This module is stateful.

local history, max, undo_at, pre_image, pre_state = {}, 32, 0
local costume_history, costume_undo_at = {}, 0

local push = function(x, hist)
   hist = hist or history
   table.insert(hist, x)
   if(#hist > max) then table.remove(hist, 1) end
end

local state = function(c)
   local s = lume.pick(c, unpack(fields))
   s.characters, s.buttons = {}, {}
   for k,ch in pairs(c.characters) do
      if(not ch.skip_save) then
         s.characters[k] = lume.pick(ch, unpack(character_fields))
      end
   end
   for k,b in pairs(c.buttons) do
      if(not b.skip_save) then
         s.buttons[k] = lume.pick(b, unpack(button_fields))
      end
   end
   return lume.serialize(s)
end

local save = function(undoing, save_card, force)
   local c = save_card or card.current()
   if(c.image_changed or force) then c.image_file = c.name..".png" end
   if(c.changed or force) then
      love.filesystem.write(c.stack_meta.dir .. "/" .. c.name .. ".lua",
                            "return " .. state(c))
      c.changed = false
      if(not undoing) then
         undo_at = 0
         push(pre_state)
      end
   end
   if(c.image_changed or force) then
      local data = c.image:getData():encode("png")
      love.filesystem.write(c.stack_meta.dir .. "/" .. c.name .. ".png",
                            data:getString())
      c.image_changed = false
      if(not undoing) then
         undo_at = 0
         push({card=c.name, image_data=pre_image})
      end
   end
end

local undo = function()
   local prev = history[#history-undo_at]
   local c = card.current()
   if(undo_at < #history) then undo_at = undo_at + 1 end
   if(not prev) then return
   elseif(prev.image_data) then
      card.find(prev.card).image = love.graphics.newImage(prev.image_data)
      c.image_changed = true
      save(true)
   -- elseif(type(prev[1] == "number")) then
   --    print("color")
   --    local color = require("modes.color")
   --    lume.clear(color)
   --    lume.extend(color, prev)
   elseif(prev) then
      local prev_card = card.find(prev.name)
      for k,v in pairs(prev) do prev_card[k] = v end
      prev_card.changed = true
      save(true, prev_card)
   end
end

return {
   state = state,

   save = save,

   pre_image = function()
      pre_image = card.current().image:getData():encode("png")
   end,

   pre_state = function(field)
      local trimmed = lume.pick(card.current(), "name", field)
      if(field == "characters") then
         for i,ch in pairs(trimmed.characters) do
            trimmed.characters[i] = lume.pick(ch, unpack(character_fields))
         end
      elseif(field == "buttons") then
         for i,b in pairs(trimmed.buttons) do
            trimmed.buttons[i] = lume.pick(b, unpack(button_fields))
         end
      end
      pre_state = lume.deserialize(lume.serialize(trimmed))
   end,

   costume = function(character, image)
      local data = image:getData():encode("png")
      local dir = card.current().stack_meta.dir .. "/costumes/"
      love.filesystem.write(dir .. character.name .. "_" .. character.costume
                               .. ".png", data:getString())
   end,

   pre_costume = function(c)
      push(c.image:getData():encode("png"), costume_history)
      costume_undo_at = 0
   end,

   new_costume = function() lume.clear(costume_history) end,

   undo_costume = function(c)
      local prev = costume_history[#costume_history - costume_undo_at]
      if(costume_undo_at < #costume_history) then
         costume_undo_at = costume_undo_at + 1
      end
      if(prev) then
         c.image:getData():paste(love.image.newImageData(prev),
                                 0, 0, 0, 0, c.w, c.h)
         c.image:refresh()
      end
   end,

   undo = undo,
   history = history, -- for debug
}
