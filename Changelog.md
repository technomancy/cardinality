# Change Log

## 0.1.1 - 2016-11-10

* Fix a bug throwing off text alignment in alerts.

## 0.1.0 - 2016-11-09

* Initial release.
* Cards with buttons and characters.
* Pencil, fill, line, color, and dropper drawing tools.
* Simplistic event handler API (mouse, key, enter, and update).
* Stack creation, loading, and copying.
