local radial = require("radial")
local menu = require("menu")
local button = require("button")
local card = require("card")
local character = require("character")

return function(show_buttons)
   local c = card.current()
   local canvas = love.graphics.newCanvas(c.stack_meta.w, c.stack_meta.h)
   canvas:setFilter("nearest", "nearest")
   love.graphics.setCanvas(canvas)
   love.graphics.setColor(255, 255, 255)
   if(c.image_file and not c.image) then
      c.image = love.graphics.newImage(c.stack_meta.dir.."/"..c.image_file)
      c.image:setFilter('nearest', 'nearest')
   elseif(not c.image) then
      local data = love.image.newImageData(c.stack_meta.w, c.stack_meta.h)
      c.image = love.graphics.newImage(data)
      c.image:setFilter('nearest', 'nearest')
   end
   love.graphics.setColor(255, 255, 255)
   love.graphics.draw(c.image, 0, 0)
   for _,btn in pairs(c.buttons) do button.draw(btn, show_buttons) end
   for _,char in pairs(c.characters or {}) do character.draw(char) end
   love.graphics.setCanvas()
   love.graphics.draw(canvas, 0, 0, 0, c.stack_meta.scale, c.stack_meta.scale)
   radial.draw(menu)
end
