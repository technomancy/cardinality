local editor = require("polywell")
local utils = require("polywell.utils")
local inspect = require("lib.inspect")
local lume = require("lib.lume")

local fs = love.filesystem

-- Heads up! This module is stateful.

local current, stack, meta = nil, {}, nil

local new = function(name)
   return {name = assert(name), buttons = {}, characters = {}, type="card",
           code = "local this_card = ...\n", stack_meta = meta,}
end

local is_on_edge = function(card, ch)
   if(ch.y <= 0) then return "top"
   elseif(ch.x <= 0) then return "left"
   elseif(ch.y + (ch.h or 32) > card.stack_meta.h) then return "bottom"
   elseif(ch.x + (ch.w or 32) > card.stack_meta.w) then return "right" end
end

local event_handler = function(event)
   return function(...)
      local c = stack[current]
      if(c[event]) then c[event](...) end
      for _,ch in pairs(c.characters) do
         if(ch[event]) then ch[event](...) end
      end
   end
end

local on_enter = event_handler("on_enter")
local update = event_handler("on_update")

local mouse_event_handler = function(event)
   return function(x, y, ...)
      local c = stack[current]
      if(c[event]) then c[event](x, y, ...) end
      for _,ch in pairs(c.characters) do
         if(ch[event] and x > ch.x and x <= ch.x + ch.w and
            y > ch.y and y <= ch.y + ch.h) then
            ch[event](x, y, ...) end
      end
   end
end

local jump_to = function(name, characters)
   local card = assert(stack[name], "No such card: " .. name)
   local execute = require("execute") -- circular dependency
   local from = current
   current = name
   editor.open(nil, meta.dir .. "/" .. name .. ".lua", "play")
   execute.load(card, card)
   for _,c in ipairs(characters or {}) do
      if(stack[from].characters[c.name]) then
         stack[from].characters[c.name], card.characters[c.name] = nil, c
      end
   end
   for _,c in pairs(card.characters) do
      execute.load(c, c, card)
   end
   on_enter(stack[current])
end

local load = function(dir)
   local index = love.filesystem.load(dir.."/index.lua")()
   lume.clear(stack)
   meta = love.filesystem.load(dir.."/meta.lua")()
   meta.dir = dir
   for _,card_name in pairs(index) do
      if(love.filesystem.isFile(dir.."/"..card_name..".lua")) then
         stack[card_name] = love.filesystem.load(dir.."/"..card_name..".lua")()
      else
         stack[card_name] = new(card_name)
      end
      assert(card_name == stack[card_name].name, "Misnamed card: " ..
                card_name .. " vs " .. stack[card_name].name)
      stack[card_name].stack_meta = meta
   end
   if(meta.w and meta.h) then
      local s = meta.scale or 1
      love.window.setMode(meta.w * s, meta.h * s)
   end

   jump_to(index[1])
end

return {
   load = load,

   new = new,

   create = function(name)
      local index = love.filesystem.load(meta.dir.."/index.lua")()
      table.insert(index, name)
      love.filesystem.write(meta.dir.."/index.lua", "return "..inspect(index))
      stack[name], current = new(name), name
   end,

   jump_to = jump_to,

   jump_select = function()
      local completer = function(input)
         return utils.completions_for(input, lume.keys(stack))
      end
      editor.read_line("Jump to: ",
                       function(target, cancel)
                          if(not cancel) then jump_to(target) end end,
                       {completer=completer, moused=true})
   end,

   current = function() return stack[current] end,

   all_names = function() return lume.keys(stack) end,

   scale = function(c, x, y)
      if(c.stack_meta.scale) then
         return x / c.stack_meta.scale, y / c.stack_meta.scale
      else
         return x, y
      end
   end,

   find = function(name) return assert(stack[name], "Not found:" .. name) end,

   on_mouse_pressed = mouse_event_handler("on_mouse_pressed"),
   on_mouse_released = mouse_event_handler("on_mouse_released"),
   on_mouse_moved = mouse_event_handler("on_mouse_moved"),
   on_key_pressed = event_handler("on_key_pressed"),

   update = function(dt)
      if(editor.current_mode_name() == "play") then update(dt) end
   end,

   on_move = function(ch)
      local c = stack[current]
      if(c.on_edge) then
         local edge = is_on_edge(c, ch)
         if(edge) then return c.on_edge(ch, edge) end
      end
   end,

   new_stack = function()
      editor.read_line("New stack name: ",
                       function(name, cancel)
                          if(cancel) then return end
                          assert(not fs.isDirectory(name),
                                 name .. " already exists!")
                          fs.createDirectory(name)
                          fs.createDirectory(name .. "/costumes")
                          fs.write(name .. "/index.lua", "return {'start'}")
                          fs.write(name .. "/meta.lua", "return " ..
                                      lume.serialize(meta))
                          fs.write(name .. "/start.lua", "return " ..
                                      lume.serialize(new("start")))
                          load(name)
                       end, {moused=true})
   end,

   stack_completer = function(input)
      local all, stacks = fs.getDirectoryItems(""), {}
      for _,i in lume.ripairs(all) do
         if(fs.isFile(i .. "/index.lua")) then table.insert(stacks, i) end
      end
      return utils.completions_for(input, stacks)
   end,

   copy_stack = function(from, to)
      assert(not fs.isDirectory(to), to .. " already exists!")
      fs.createDirectory(to)
      fs.createDirectory(to .. "/costumes")
      for _,file in pairs(fs.getDirectoryItems(from)) do
         if(fs.isFile(from .. "/" .. file)) then
            fs.write(to .. "/" .. file, fs.read(from .. "/" .. file))
         end
      end
      for _,file in pairs(fs.getDirectoryItems(from .. "/costumes")) do
         if(fs.isFile(from .. "/" .. file)) then
            fs.write(to .. "/costumes/" .. file,
                     fs.read(from .. "/costumes/" .. file))
         end
      end
   end,
}
