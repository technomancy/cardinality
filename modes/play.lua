local editor = require("polywell")
local lume = require("lib.lume")
local draw = require("draw")
local radial = require("radial")
local menu = require("menu")
local button = require("button")
local card = require("card")
local execute = require("execute")
local inspect = require("lib.inspect")
local save = require("save")

editor.define_mode("play", "default", {draw=draw,cursor="hand"})

local is_play = function() return editor.current_mode_name() == "play" end

require("polywell.config.console")
local close = function() editor.change_buffer(editor.last_buffer()) end
editor.bind("play", "ctrl-return", lume.fn(editor.change_buffer, "*console*"))
editor.bind("play", "ctrl-z", save.undo)

editor.bind("play", "ctrl-0", lume.fn(card.load, "main"))

editor.bind("console", "escape", close)
editor.bind("console", "ctrl-return", close)
editor.with_current_buffer("*console*",
                           function()
                              editor.set_prop("loadstring", execute.loadstring)
                              editor.set_prop("pps", inspect)
                           end)

local target_button

editor.bind("play", "mouse_pressed", function(x, y, btn)
               if(btn == 1) then
                  x, y = card.scale(card.current(), x, y)
                  if(is_play()) then card.on_mouse_pressed(x, y) end
                  target_button = button.find(card.current(), x, y)
               elseif(btn == 2) then
                  radial.mousepressed(menu, x, y)
               end
end)

editor.bind("play", "mouse_released", function(x, y, btn)
               if(btn == 1) then
                  x, y = card.scale(card.current(), x, y)
                  if(is_play()) then card.on_mouse_released(x, y) end
                  if(button.find(card.current(), x, y) == target_button and
                     target_button) then
                     button.activate(target_button)
                  end
               elseif(btn == 2) then
                  radial.mousereleased(menu, x, y)
               end
end)

editor.bind("play", "mouse_moved", function(x, y, dx, dy)
               if(love.mouse.isDown(2)) then
                  radial.mousemoved(menu, x, y, dx, dy)
               elseif(love.mouse.isDown(1) and is_play()) then
                  card.on_mouse_moved(x, y)
               end
end)

editor.bind("play", "ctrl-q", love.event.quit)

editor.bind("play", "__any", function(key)
               if(key == "mouse_focus") then return end -- there are others?
               if(is_play()) then card.on_key_pressed(key) end
end)
