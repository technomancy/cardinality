local editor = require("polywell")
local radial = require("radial")
local menu = require("menu")
local draw = require("draw")
local card = require("card")
local save = require("save")
local color = require("modes.color")

local start, current

local line_draw = function()
   draw()
   if(start) then
      local s = card.current().stack_meta.scale
      love.graphics.setColor(color)
      love.graphics.line(start[1] * s, start[2] * s, current[1] * s, current[2] * s)
   end
end

editor.define_mode("line", "play", {draw=line_draw, cursor="crosshair"})

editor.bind("line", "mouse_pressed", function(x, y, btn)
               if(btn == 1) then
                  local c = card.current()
                  x, y = card.scale(c, x, y)
                  start, current = {x, y}, {x, y}
                  save.pre_image()
               elseif(btn == 2) then
                  radial.mousepressed(menu, x, y)
               end
end)

editor.bind("line", "mouse_released", function(x, y, btn)
               if(btn == 1) then
                  local c = card.current()
                  local canvas = love.graphics.newCanvas(c.stack_meta.w,
                                                         c.stack_meta.h)
                  love.graphics.setLineStyle("rough")
                  love.graphics.setCanvas(canvas)
                  love.graphics.setColor(255, 255, 255)
                  love.graphics.draw(c.image, 0, 0)
                  love.graphics.setColor(color)
                  love.graphics.line(start[1], start[2], current[1],
                                     current[2])
                  love.graphics.setCanvas()
                  local data = canvas:newImageData()
                  c.image:getData():paste(data, 0, 0, 0, 0, c.w, c.h)
                  c.image:refresh()
                  start, c.image_changed = nil, true
                  save.save()
               elseif(btn == 2) then
                  radial.mousereleased(menu, x, y)
               end
end)

editor.bind("line", "mouse_moved", function(x, y, dx, dy)
               if(love.mouse.isDown(1)) then
                  local c = card.current()
                  x, y = card.scale(c, x, y)
                  current = {x, y}
               elseif(love.mouse.isDown(2)) then
                  radial.mousemoved(menu, x, y, dx, dy)
               end
end)
