local editor = require("polywell")
local radial = require("radial")
local menu = require("menu")
local character = require("character")
local character_edit = require("modes.character_edit")
local costume = require("modes.costume")
local card = require("card")
local save = require("save")
local lume = require("lib.lume")
local draw = require("draw")
local corner = require("corner")

local double_click_time, target, pre, last_click = 0.3

local double_clicked = function()
   return last_click and love.timer.getTime() - last_click < double_click_time
end

local create_character = function(x, y, c, name, cancel)
   if(not cancel) then
      c.characters[name] = character.new(name, x, y)
      c.changed = true
      save.save()
   end
end

local corner_menu = {
   {color={200,5,120}, callback=function(ch)
       character_edit(card.current(), ch)
   end,
    image_file="assets/code.png",},
   {color={250,200,200}, callback=costume,
   image_file="assets/pencil.png",},
   {color={230,5,5}, callback=function(ch)
       lume.remove(card.current().characters, ch)
   end, image_file="assets/delete.png",},
}

local character_draw = function()
   draw()
   if(target) then corner.draw(corner_menu) end
end

editor.define_mode("character", "play", {draw=character_draw})

editor.bind("character", "mouse_pressed", function(x, y, btn)
               if(btn == 1) then
                  local c = card.current()
                  x, y = card.scale(card.current(), x, y)
                  save.pre_state("characters")
                  target = character.find(c, x, y)
                  if(double_clicked()) then
                     if(target) then
                        character_edit(c, target)
                     else
                        editor.read_line("New character name: ",
                                         lume.fn(create_character, x, y, c),
                                         {moused=true})
                     end
                     target = nil
                  elseif(target) then
                     pre = lume.pick(target, "x", "y")
                  end
                  last_click = love.timer.getTime()
               elseif(btn == 2) then
                  radial.mousepressed(menu, x, y)
               end
end)

editor.bind("character", "mouse_released", function(x, y, btn)
               if(btn == 1 and target) then
                  if(corner.mouse_released(corner_menu, target, x, y)) then
                     lume.extend(target, pre)
                  end
                  target.x, target.y = math.floor(target.x), math.floor(target.y)
                  card.current().changed = true
                  save.save()
                  target, pre = nil, nil
               elseif(btn == 2) then
                  radial.mousereleased(menu, x, y)
               end
end)

editor.bind("character", "mouse_moved", function(x, y, dx, dy)
               if(love.mouse.isDown(1) and target) then
                  dx, dy = card.scale(card.current(), dx, dy)
                  character.move(target, dx, dy)
               elseif(love.mouse.isDown(2)) then
                  radial.mousemoved(menu, x, y, dx, dy)
               end
end)
