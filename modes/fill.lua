local editor = require("polywell")
local radial = require("radial")
local menu = require("menu")
local draw = require("draw")
local card = require("card")
local save = require("save")
local lume = require("lib.lume")
local color = require("modes.color")

local pixel_for = function(c, x, y)
   return c.image:getData():getPixel(math.floor(x), math.floor(y))
end

local function fill(c, x, y, p, original)
   local w, h = c.stack_meta.w, c.stack_meta.h
   local current = table.concat({p(x, y)}, " ")
   if(current == original and current ~= table.concat(color, " ")) then
      c.image_changed = true
      c.image:getData():setPixel(x, y, unpack(color))
      if(x > 0) then fill(c, x-1, y, p, original) end
      if(x < w-1) then fill(c, x+1, y, p, original) end
      if(y > 0) then fill(c, x, y-1, p, original) end
      if(y < h-1) then return fill(c, x, y+1, p, original) end
   end
end

editor.define_mode("fill", "play", {draw=draw, cursor={"fill", 12, 12}})

editor.bind("fill", "mouse_pressed", function(x, y, btn)
               if(btn == 1) then save.pre_image() end
               if(btn == 2) then radial.mousepressed(menu, x, y) end
end)
editor.bind("fill", "mouse_released", function(x, y, btn)
               if(btn == 1) then
                  local c = card.current()
                  x, y = card.scale(card.current(), x, y)
                  local original = table.concat({pixel_for(c, x, y)}, " ")
                  love.graphics.setColor(color)
                  -- TODO: should actually avoid stack overflow here!
                  pcall(fill, c, math.floor(x), math.floor(y),
                        lume.fn(pixel_for, c), original)
                  c.image:refresh()
                  save.save()
               elseif(btn == 2) then radial.mousereleased(menu, x, y) end
end)

editor.bind("fill", "mouse_moved", function(x, y)
               if(love.mouse.isDown(2)) then radial.mousemoved(menu, x, y) end
end)
