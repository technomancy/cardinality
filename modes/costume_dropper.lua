local editor = require("polywell")
local lume = require("lib.lume")
local radial = require("radial")
local menu = require("modes.costume_menu")
local color = require("modes.color")

editor.define_mode("costume_dropper", "costume", {cursor={"dropper",3,11}})

editor.bind("costume_dropper", "mouse_pressed", function(x, y, btn)
               if(btn == 2) then radial.mousepressed(menu, x, y) end
end)
editor.bind("costume_dropper", "mouse_released", function(x, y, btn)
               if(btn == 2) then radial.mousereleased(menu, x, y)
               elseif(btn == 1) then
                  local c = editor.get_prop("costume")
                  x = math.floor(x / c.scale - c.margin)
                  y = math.floor(y / c.scale - c.margin)
                  if(x >= 0 and x < c.w and y >= 0 and y < c.h) then
                     lume.clear(color)
                     lume.extend(color, {c.image:getData():getPixel(x, y)})
                  end
               end
end)

editor.bind("costume_dropper", "mouse_moved", function(x, y, dx, dy)
               if(love.mouse.isDown(2)) then
                  radial.mousemoved(menu, x, y, dx, dy)
               end
end)
