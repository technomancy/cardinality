local editor = require("polywell")
local radial = require("radial")
local menu = require("modes.costume_menu")
local lume = require("lib.lume")
local save = require("save")
local color = require("modes.color")

local pixel_for = function(c, x, y)
   return c.image:getData():getPixel(math.floor(x), math.floor(y))
end

local function fill(c, x, y, p, original)
   local w, h = c.w, c.h
   local current = table.concat({p(x, y)}, " ")
   if(current == original and current ~= table.concat(color, " ")) then
      c.image_changed = true
      c.image:getData():setPixel(x, y, unpack(color))
      if(x > 0) then fill(c, x-1, y, p, original) end
      if(x < w-1) then fill(c, x+1, y, p, original) end
      if(y > 0) then fill(c, x, y-1, p, original) end
      if(y < h-1) then return fill(c, x, y+1, p, original) end
   end
end

editor.define_mode("costume_fill", "costume", {cursor={"fill", 12, 12}})

editor.bind("costume_fill", "mouse_pressed", function(x, y, btn)
               if(btn == 1) then save.pre_costume(editor.get_prop("costume"))
               elseif(btn == 2) then radial.mousepressed(menu, x, y) end
end)
editor.bind("costume_fill", "mouse_released", function(x, y, btn)
               if(btn == 1) then
                  local c = editor.get_prop("costume")
                  x, y = x / c.scale - c.margin, y / c.scale - c.margin
                  if(x >= 0 and x < c.w and y >= 0 and y < c.h) then
                     local original = table.concat({pixel_for(c, x, y)}, " ")
                     love.graphics.setColor(color)
                     pcall(fill, c, math.floor(x), math.floor(y),
                           lume.fn(pixel_for, c), original)
                     c.image:refresh()
                  end
               elseif(btn == 2) then radial.mousereleased(menu, x, y) end
end)

editor.bind("costume_fill", "mouse_moved", function(x, y)
               if(love.mouse.isDown(2)) then radial.mousemoved(menu, x, y) end
end)
