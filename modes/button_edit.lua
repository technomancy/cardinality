local editor = require("polywell")
local lume = require("lib.lume")
local save = require("save")
local card = require("card")

require("polywell.config.edit")

editor.define_mode("button_edit", "lua")
editor.bind("button_edit", "escape", function()
               card.current().changed = true
               editor.save()
               save.save()
               editor.activate_mode("buttons")
end)

local fs_for = function(c)
   local fs = {}
   local mt = {
      __index = function(_, path)
         local bi = tonumber(lume.split(path, "/")[2])
         return c.buttons[bi] and c.buttons[bi].code
      end,
      __newindex = function(_, path, contents)
         local bi = tonumber(lume.split(path, "/")[2])
         local button = c.buttons[bi]
         if(button) then
            button.code = contents
         else
            print("No button!", path)
         end
      end,
      __separator = " "
   }
   setmetatable(fs, mt)
   return fs
end

return function(c, button)
   local button_index = lume.find(c.buttons, button)
   editor.open(fs_for(c), c.name .. "/" .. button_index, "button_edit")
end
