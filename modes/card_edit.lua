local editor = require("polywell")
local save = require("save")
local card = require("card")
local lume = require("lib.lume")
local execute = require("execute")

require("polywell.config.edit")

local prev_mode

editor.define_mode("card_edit", "lua")
editor.bind("card_edit", "escape", function()
               editor.save()
               save.save()
               execute.load(card.current(), card.current())
               editor.activate_mode(prev_mode)
end)

editor.bind("edit", "ctrl-return", lume.fn(editor.change_buffer, "*console*"))

local fs_for = function(c)
   local fs = {}
   local mt = {
      __index = function(_, _) return c.code end,
      __newindex = function(_, _, contents) c.code = contents end,
      __separator = " "
   }
   setmetatable(fs, mt)
   return fs
end

return function(c)
   prev_mode = editor.current_mode_name()
   editor.open(fs_for(c), c.name, "card_edit")
end
