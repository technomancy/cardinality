local editor = require("polywell")
local radial = require("radial")
local menu = require("modes.costume_menu")
local color = require("modes.color")
local save = require("save")

local pencilate = function(c, x, y)
   c.image_changed = true
   c.image:getData():setPixel(math.floor(x), math.floor(y), unpack(color))
   c.image:refresh()
end

editor.define_mode("costume_pencil", "costume", {cursor={"pencil",2,13}})

editor.bind("costume_pencil", "mouse_pressed", function(x, y, btn)
               if(btn == 1) then save.pre_costume(editor.get_prop("costume"))
               elseif(btn == 2) then radial.mousepressed(menu, x, y) end
            end)

editor.bind("costume_pencil", "mouse_released", function(x, y, btn)
               if(btn == 1) then
                  local c = editor.get_prop("costume")
                  x, y = x / c.scale - c.margin, y / c.scale - c.margin
                  if(x >= 0 and x < c.w and
                        y >= 0 and y < c.h) then
                     pencilate(c, x, y)
                  end
                  save.costume(c.character, c.image)
               elseif(btn == 2) then radial.mousereleased(menu, x, y) end
end)

editor.bind("costume_pencil", "mouse_moved", function(x, y, dx, dy)
               if(love.mouse.isDown(1)) then
                  local c = editor.get_prop("costume")
                  x, y = x / c.scale - c.margin, y / c.scale - c.margin
                  if(x >= 0 and x < c.w and
                        y >= 0 and y < c.h) then
                     pencilate(c, x, y)
                  end
               elseif(love.mouse.isDown(2)) then
                  radial.mousemoved(menu, x, y, dx, dy)
               end
end)
