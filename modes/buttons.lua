local editor = require("polywell")
local utils = require("polywell.utils")
local draw = require("draw")
local radial = require("radial")
local menu = require("menu")
local button = require("button")
local card = require("card")
local lume = require("lib.lume")
local save = require("save")
local corner = require("corner")
local button_edit = require("modes.button_edit")

local resize_zone, double_click_time = 3, 0.3
local min_w, min_h = 10, 10
local target_button, pre_button, last_click, action

local change_button = function(btn, dx, dy)
   if(action == "move") then
      btn.x, btn.y = btn.x + dx, btn.y + dy
   else
      local neww, newh
      if(action == "rx" or action == "resize") then neww = btn.w + dx end
      if(action == "ry" or action == "resize") then newh = btn.h + dy end
      if(neww and neww >= min_w) then btn.w = neww end
      if(newh and newh >= min_h) then btn.h = newh end
   end
end

local set_jump_to = function(btn, target, cancel)
   if(not cancel) then
      btn.meta.jump_to = target ~= "NONE" and target
      card.current().changed = true
      save.save()
   end
end

local completer = function(input)
   local choices = card.all_names()
   table.insert(choices, "NONE")
   return utils.completions_for(input, choices)
end

local corner_menu = {
   {color={200,5,120}, callback=function(btn)
       button_edit(card.current(), btn)
   end,
    image_file="assets/code.png",},
   {color={20,100,255}, callback=function(btn)
       editor.read_line("Set jump to: ",
                        lume.fn(set_jump_to, btn),
                        {completer=completer, moused=true})
   end,
    image_file="assets/stack.png",},
   {color={230,5,5}, callback=function(btn)
       lume.remove(card.current().buttons, btn)
       card.current().changed = true
       save.save()
   end,
    image_file="assets/delete.png",},
}

local button_draw = function()
   draw(true)
   if(target_button) then corner.draw(corner_menu) end
end

editor.define_mode("buttons", "play", {draw=button_draw})

local double_clicked = function()
   return last_click and love.timer.getTime() - last_click < double_click_time
end

local action_for = function(btn, x, y)
   local rx = x < btn.x + resize_zone or x > btn.x + btn.w - resize_zone
   local ry = y < btn.y + resize_zone or y > btn.y + btn.h - resize_zone
   return (rx and ry and "resize") or (rx and "rx") or (ry and "ry") or "move"
end

local create_button = function(x, y, c, name, cancel)
   if(not cancel) then
      table.insert(c.buttons, button.new(name, x, y))
      card.current().changed = true
      save.save()
   end
end

editor.bind("buttons", "mouse_pressed", function(x, y, btn)
               if(btn == 1) then
                  save.pre_state("buttons")
                  local c = card.current()
                  x, y = card.scale(c, x, y)
                  target_button = button.find(c, x, y, true)
                  if(double_clicked()) then
                     if(target_button) then
                        button_edit(c, target_button)
                        target_button = nil
                     else
                        editor.read_line("New button label: ",
                                         lume.fn(create_button, x, y, c),
                                         {moused=true})
                     end
                  elseif(target_button) then
                     action = action_for(target_button, x, y)
                     pre_button = lume.pick(target_button, "x", "y", "w", "h")
                  end
                  last_click = love.timer.getTime()
               elseif(btn == 2) then
                  radial.mousepressed(menu, x, y)
               end
end)

editor.bind("buttons", "mouse_released", function(x, y, btn)
               if(btn == 1 and target_button) then
                  if(action == "move") then
                     if(corner.mouse_released(corner_menu, target_button, x, y)) then
                        lume.extend(target_button, pre_button)
                     else
                        target_button.x = math.floor(target_button.x)
                        target_button.y = math.floor(target_button.y)
                        target_button.w = math.floor(target_button.w)
                        target_button.h = math.floor(target_button.h)
                        card.current().changed = true
                        save.save()
                     end
                  end
                  target_button, pre_button = nil, nil
               elseif(btn == 2) then
                  radial.mousereleased(menu, x, y)
               end
end)

editor.bind("buttons", "mouse_moved", function(x, y, dx, dy)
               if(love.mouse.isDown(1) and target_button) then
                  local c = card.current()
                  change_button(target_button, card.scale(c, dx, dy))
               elseif(love.mouse.isDown(2)) then
                  radial.mousemoved(menu, x, y, dx, dy)
               end
end)
