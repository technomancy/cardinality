local editor = require("polywell")
local save = require("save")
local card = require("card")
local radial = require("radial")
local menu = require("modes.costume_menu")

local scale, last_mode, w, h, image

local margin = 16

local draw = function()
   love.graphics.push()
   love.graphics.scale(scale, scale)
   local square_size = 8
   -- checkered background
   for x=margin,margin+w-1,square_size do
      for y=margin,margin+w-1,square_size do
         if(math.mod(x/square_size+y/square_size, 2) == 0) then
            love.graphics.setColor({100, 100, 100})
         else
            love.graphics.setColor({200, 200, 200})
         end
         love.graphics.rectangle("fill", x, y, square_size, square_size)
      end
   end
   love.graphics.setColor({255,255,255})
   love.graphics.draw(image, margin, margin)
   love.graphics.pop()
   radial.draw(menu)
end

editor.define_mode("costume", "play", {draw=draw})

local save_costume = function()
   local costume = editor.get_prop("costume")
   save.costume(costume.character, costume.image)
end

editor.bind("costume", "backspace",
            function() editor.activate_mode(last_mode) end)
editor.bind("costume", "escape",
            function() save_costume() editor.activate_mode(last_mode) end)
editor.bind("costume", "ctrl-s", save_costume)
editor.bind("costume", "ctrl-z", function()
               save.undo_costume(editor.get_prop("costume"))
end)

return function(ch)
   last_mode = editor.mode().name
   image = ch.costume_images[ch.costume]
   save.new_costume()
   w, h = image:getDimensions()
   scale = card.current().stack_meta.scale * 2
   editor.activate_mode("costume_pencil")
   editor.set_prop("costume", {image=image, w=w, h=h, character=ch,
                               scale=scale, margin=margin})
end
