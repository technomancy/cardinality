local editor = require("polywell")
local radial = require("radial")
local menu = require("modes.costume_menu")
local save = require("save")
local color = require("modes.color")

local start, current

local line_draw = function()
   if(start) then
      local margin = editor.get_prop("costume").margin
      local s = editor.get_prop("costume").scale
      love.graphics.setColor(color)
      love.graphics.push()
      love.graphics.translate(margin*s, margin*s)
      love.graphics.line(start[1] * s, start[2] * s,
                         current[1] * s, current[2] * s)
      love.graphics.pop()
   end
end

editor.define_mode("costume_line", "costume", {over_draw = line_draw,
                                               cursor="crosshair"})

editor.bind("costume_line", "mouse_pressed", function(x, y, btn)
               if(btn == 1) then
                  local c = editor.get_prop("costume")
                  x, y = x / c.scale - c.margin, y / c.scale - c.margin
                  start, current = {x, y}, {x, y}
                  save.pre_costume(editor.get_prop("costume"))
               elseif(btn == 2) then
                  radial.mousepressed(menu, x, y)
               end
end)

editor.bind("costume_line", "mouse_released", function(x, y, btn)
               if(btn == 1) then
                  local c = editor.get_prop("costume")
                  local canvas = love.graphics.newCanvas(c.w, c.h)
                  love.graphics.setLineStyle("rough")
                  love.graphics.setCanvas(canvas)
                  love.graphics.setColor(255, 255, 255)
                  love.graphics.draw(c.image, 0, 0)
                  love.graphics.setColor(color)
                  love.graphics.line(start[1], start[2], current[1],
                                     current[2])
                  love.graphics.setCanvas()
                  local data = canvas:newImageData()
                  c.image:getData():paste(data, 0, 0, 0, 0, c.w, c.h)
                  c.image:refresh()
                  start, c.image_changed = nil, true
                  -- save.save()
               elseif(btn == 2) then
                  radial.mousereleased(menu, x, y)
               end
end)

editor.bind("costume_line", "mouse_moved", function(x, y, dx, dy)
               if(love.mouse.isDown(1)) then
                  local c = editor.get_prop("costume")
                  x, y = x / c.scale - c.margin, y / c.scale - c.margin
                  current = {x, y}
               elseif(love.mouse.isDown(2)) then
                  radial.mousemoved(menu, x, y, dx, dy)
               end
end)
