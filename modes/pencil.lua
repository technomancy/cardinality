local editor = require("polywell")
local radial = require("radial")
local menu = require("menu")
local draw = require("draw")
local card = require("card")
local save = require("save")
local color = require("modes.color")

local pencilate = function(c, x, y)
   c.image_changed = true
   c.image:getData():setPixel(math.floor(x), math.floor(y), unpack(color))
   c.image:refresh()
end

editor.define_mode("pencil", "play", {draw=draw,cursor={"pencil",2,13}})

editor.bind("pencil", "mouse_pressed", function(x, y, btn)
               if(btn == 1) then save.pre_image()
               elseif(btn == 2) then radial.mousepressed(menu, x, y) end
            end)

editor.bind("pencil", "mouse_released", function(x, y, btn)
               if(btn == 1) then
                  local c = card.current()
                  x, y = card.scale(c, x, y)
                  pencilate(c, x, y)
                  c.image_changed = true
                  save.save()
               elseif(btn == 2) then radial.mousereleased(menu, x, y) end
end)

editor.bind("pencil", "mouse_moved", function(x, y, dx, dy)
               if(love.mouse.isDown(1)) then
                  local c = card.current()
                  x, y = card.scale(c, x, y)
                  pencilate(c, x, y)
               elseif(love.mouse.isDown(2)) then
                  radial.mousemoved(menu, x, y, dx, dy)
               end
end)
