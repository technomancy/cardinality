local editor = require("polywell")
local lume = require("lib.lume")
local radial = require("radial")
local menu = require("menu")
local draw = require("draw")
local card = require("card")
local color = require("modes.color")

editor.define_mode("dropper", "play", {draw=draw, cursor={"dropper",3,11}})

editor.bind("dropper", "mouse_pressed", function(x, y, btn)
               if(btn == 2) then radial.mousepressed(menu, x, y) end
end)
editor.bind("dropper", "mouse_released", function(x, y, btn)
               if(btn == 2) then radial.mousereleased(menu, x, y)
               elseif(btn == 1) then
                  local c = card.current()
                  x, y = card.scale(card.current(), x, y)
                  lume.clear(color)
                  lume.extend(color, {c.image:getData():getPixel(x, y)})
               end
end)

editor.bind("dropper", "mouse_moved", function(x, y, dx, dy)
               if(love.mouse.isDown(2)) then
                  radial.mousemoved(menu, x, y, dx, dy)
               end
end)
