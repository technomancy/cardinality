local polywell = require("polywell")
local lume = require("lib.lume")

local mode = function(m) return lume.fn(polywell.activate_mode, m) end

return {
   { {color={250,200,200}, callback = mode("costume_pencil"),
      image_file="assets/pencil.png",},
   },
   {
      { color={200,250,200}, callback = mode("costume_fill"),
        image_file="assets/fill.png",},
      { color={220,100,20}, callback = mode("costume_line"),
        image_file="assets/line.png",},
      { color={20,200,200}, callback = mode("costume_dropper"),
        image_file="assets/dropper.png",},
      { color={200,100,200}, callback = mode("color"),
        image_file="assets/colors.png",},
   },
}
