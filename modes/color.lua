local editor = require("polywell")
local lume = require("lib.lume")
local save = require("save")

local color, size, down_on = {255,255,255}, 64, nil

local colors = {
   {{0,0,0,255}, {28,43,83,255},
      {127,36,84,255}, {0,135,81,255}},
   {{171,82,54,255}, {96,88,79,255},
      {195,195,198,255}, {255,241,233,255}},
   {{237,27,81,255}, {250,162,27,255},
      {247,236,47,255}, {93,187,77,255}},
   {{81,166,220,255}, {131,118,156,255},
      {241,118,166,255}, {252,204,171,255}}}

local draw = function()
   for y,row in pairs(colors) do
      for x,col in pairs(row) do
         love.graphics.setColor(col)
         love.graphics.rectangle("fill", x*size, y*size, size, size)
      end
   end
end

local find = function(x, y)
   x, y = x - size, y - size
   if(x > 0 and x < size * #colors[1] and y > 0 and y < size * #colors) then
      local row = colors[math.ceil(y / size)]
      return row and row[math.ceil(x / size)]
   end
end

editor.define_mode("color", "play", {draw=draw, wrap=save.color_wrap})
editor.bind("color", "escape", editor.activate_mode) -- last mode
editor.bind("color", "mouse_pressed", function(x, y, btn)
               if(btn == 1) then down_on = find(x, y) end
end)
editor.bind("color", "mouse_released", function(x, y, btn)
               if(btn == 1 and down_on and
                     table.concat(down_on, " ") ==
                  table.concat(find(x, y), " ")) then
                  lume.clear(color)
                  lume.extend(color, down_on)
                  editor.activate_mode()
               end
end)

editor.bind("color", "space", function()
               color[4] = 0 -- erase
               editor.activate_mode()
end)

return color
