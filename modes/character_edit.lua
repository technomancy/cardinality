local editor = require("polywell")
local save = require("save")
local card = require("card")
local lume = require("lib.lume")
local execute = require("execute")

require("polywell.config.edit")

local character

editor.define_mode("character_edit", "lua")
editor.bind("character_edit", "escape", function()
               editor.save()
               local this_card = card.current()
               this_card.changed = true
               save.save()
               execute.load(character, character, this_card)
               editor.activate_mode("character")
end)

local fs_for = function(c)
   local fs = {}
   local mt = {
      __index = function(_, path)
         local name = lume.split(path, "/")[2]
         return c.characters[name] and c.characters[name].code
      end,
      __newindex = function(_, path, contents)
         local name = lume.split(path, "/")[2]
         local ch = assert(c.characters[name], "not found " .. name)
         ch.code = contents
      end,
      __separator = " "
   }
   setmetatable(fs, mt)
   return fs
end

return function(c, char)
   character = char
   editor.open(fs_for(c), c.name .. "/" .. char.name, "character_edit")
end
