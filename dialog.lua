local editor = require("polywell")
local card = require("card")
local button = require("button")
local lume = require("lib.lume")

local alert = function(...)
   -- TODO: word wrap, resize messages if too long
   for _,b in pairs(card.current().buttons) do
      if(b.meta.modal) then return end
   end
   local messages = {...}
   local message = table.remove(messages, 1)
   if(message) then
      local ww, wh = card.scale(card.current(), love.window.getMode())
      local x, y, w, h = 0, wh*0.8, ww, wh*0.2
      local alert = button.new(message, x, y, w, h,
                               "lume.remove(card.current().buttons, ...)\n"
                                  .. "alert(unpack(" ..
                                  lume.serialize(messages) .. "))")
      alert.bg, alert.meta.modal = {20, 20, 20, 240}, true
      table.insert(card.current().buttons, alert)
   end
end

return {
   alert = alert,

   err = function(x)
      editor.print(x)
      if(#lume.split(x, "\n") == 1) then
         local e = lume.split(x, ": ")
         alert("Error!", e[2], e[1])
      end
   end,
}
