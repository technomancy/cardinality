local size = 45

local w,h

local find = function(menu, x, y)
   if(x+y < size) then return menu[1]
   elseif((w-x)+y < size) then return menu[2]
   elseif((w-x)+(h-y) < size) then return menu[3]
   elseif(x+(h-y) < size) then return menu[4]
   end
end

local polygon_for = function(i)
   return ({
         {0,0, size,0, 0,size},
         {w,0, w,size, w-size,0},
         {w,h, w,h-size, w-size,h},
         {0,h, size,h, 0,h-size},
   })[i]
end

return {
   -- menu should be a table; one entry per corner; each entry is a table
   -- of color, callback, and image_file like menu.lua
   draw = function(menu)
      w,h = love.window.getMode()
      local selected = find(menu, love.mouse.getPosition())
      for corner, item in pairs(menu) do
         if(item == selected) then
            love.graphics.setColor({255,255,255})
         else
            love.graphics.setColor(item.color)
         end
         love.graphics.polygon("fill", unpack(polygon_for(corner)))
         if(item.image_file and not item.image) then
            item.image = love.graphics.newImage(item.image_file)
         end
         if(item.image) then
            local x1, y1, x2, y2, x3, y3 = unpack(polygon_for(corner))
            local x, y = (x1+x2+x3)/3, (y1+y2+y3)/3
            love.graphics.draw(item.image, x, y, nil, nil, nil, 8, 8)
         end
      end
   end,

   mouse_released = function(menu, button, x, y)
      local target = find(menu, x, y)
      if(target) then
         target.callback(button)
         return true
      end
   end,
}
