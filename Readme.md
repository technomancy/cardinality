# Cardinality

A [HyperCard](https://en.wikipedia.org/wiki/HyperCard)-like authoring
tool, but with elements of [Scratch](https://scratch.mit.edu). Cards
can have buttons as well as characters that can have their own images.

Download:

* [Mac OS X](https://p.hagelb.org/cardinality/Cardinality-macosx-x64.zip)
* [Windows](https://p.hagelb.org/cardinality/Cardinality-win32.zip)
* [Portable](https://p.hagelb.org/cardinality/Cardinality.love)

Windows and Mac OS X releases are standalone, but the portable `.love`
file requires having [LÖVE](https://love2d.org) version 0.10.x.

Differences from HyperCard:

* Lua instead of HyperTalk
* Color
* Cards can have characters which can react to events or interact with each other
* Doesn't have the same rich selection of UI form elements

Differences from Scratch:

* You have to type your code
* Multiple cards instead of backgrounds; each card has its own code
* Characters _belong to_ cards, giving them a sense of place, but can move between cards
* Much richer set of event handler functions

Not all of this is finished; see TODO below.

## Usage

There's a help button when you launch that explains everything through
an in-game tutorial.

Use the right mouse button to open the menu for commands and tools.

* Hand (orange): play mode
* Plus (light yellow): create new card
* Jump (dark blue): jump to any card
* Brackets (dark purple): edit card's code
* Horizontal bar (green): button mode
* Face (bright yellow): character mode
* Stop sign (deep red): quit
* Palette (light purple): select color
* Dropper (light blue): dropper tool
* Pencil (pink): set pixel
* Bucket (gray-green): fill with color
* Line (brown): draw lines

Double-clicking in button mode creates a new button. Buttons with
empty names will not be visible in play mode. Grab the edges of
the button in button mode to resize it. While dragging a button, you
can see corner menus; this lets you edit the code, set the jump
target, or delete a button.

Double-clicking in character mode creates a new character. While
dragging a character, the corner menus let you edit the code, draw
character costumes, or delete the character.

The card's code (editable with the dark purple button) runs each time
you visit the card. This is also true of all the code for the
characters on the card. But button code only runs when the button is clicked.

Pressing `ctrl-z` will undo. You can open the Lua console with
`ctrl-enter`. The console code runs inside the same context as card,
button, and character code, but you can disable the sandbox with
`nosb()` and re-enable it with `require("execute").activate_sandbox()`.

Instead of having an erase tool, you can choose "transparent" as your
paint color by pressing space in the color picker tool, then use the
pencil, fill, etc.

## Developing

If you're running from a checkout of the source, you will need to
symlink the `main` directory into your LÖVE save dir (often
`~/.local/share/love/cardinality`) in order for your changes to that
stack to be saved.

## TODO:

* API reference
* Character collisions
* Rename character
* Re-label button
* Resize costumes
* Multiple costumes
* Menus (dialogs with with selections)
* Circle, rectangle mode

## Licenses

Copyright © 2016 Phil Hagelberg

Distributed under the GNU General Public License version 3 or later; see file LICENSE.

Uses 3rd-party libraries [Lume](https://github.com/rxi/lume) and
[inspect.lua](https://github.com/kikito/inspect.lua),
licensed under the MIT license; see file lib/MIT-LICENSE.txt.
