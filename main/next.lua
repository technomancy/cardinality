return {["buttons"]={[1]={["x"]=5.5,["code"]="local button, card = ...",["y"]=7.25,["meta"]={["jump_to"]="help-button3"},["label"]="back",["w"]=100,["type"]="button",["h"]=20},[2]={["x"]=194,["code"]="local button, card = ...",["y"]=5,["meta"]={["jump_to"]="character2"},["label"]="next",["w"]=100,["type"]="button",["h"]=20}},["code"]="alert(\"The yellow smiley tool is for characters.\",\
  \"Select it and double-click to create one.\",\
  \"You can move it around with this tool.\",\
  \"Drag it to the top right to draw on it.\")\
",["image_file"]="next.png",["characters"]={["bob"]={["x"]=238,["code"]="local character, card = ...",["costume"]=1,["y"]=31,["type"]="character",["name"]="bob",["meta"]={}}},["name"]="next",["type"]="card"}