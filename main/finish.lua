return {["buttons"]={[1]={["x"]=7,["code"]="local button, card = ...",["y"]=117,["meta"]={["jump_to"]="start"},["label"]="done",["w"]=100,["type"]="button",["h"]=20}},["code"]="local card = ...\
",["image_file"]="finish.png",["characters"]={["hello"]={["x"]=232,["code"]="local ch = ...\
\
local speed = 64\
\
ch.y = 150\
\
-- Surprise! You can also set a character's on_udpate function.\
-- It gets as an argument the amount of time it's been since the last\
-- time the update function was called.\
ch.on_update = function(time_since_last)\
  ch.y = ch.y - time_since_last * speed\
  if(ch.y < -32) then\
    ch.y = 150\
  end\
end",["costume"]=1,["y"]=26,["type"]="character",["name"]="hello",["meta"]={}}},["name"]="finish",["type"]="card"}