return {["buttons"]={[1]={["x"]=4,["code"]="local button, card = ...",["y"]=7,["meta"]={["jump_to"]="help3"},["label"]="back",["w"]=100,["type"]="button",["h"]=20},[2]={["x"]=194,["code"]="local button, card = ...",["y"]=6,["meta"]={["jump_to"]="help-button3"},["label"]="next",["w"]=100,["type"]="button",["h"]=20},[3]={["x"]=107,["code"]="-- This is the code editor for buttons!\
-- You can write any Lua code in here, and it will be run when the button is clicked.\
\
-- Lines like this that start with -- are comments; you can write whatever you want here\
-- and the program will ignore it, so it's good for explaining things.\
\
-- The function that shows the messages you've been seeing so far is called `alert'.\
\
-- Here's an example:\
\
alert(\"The demo button has some code in it.\")\
\
-- Don't worry if you don't understand the code now.\
\
-- Press escape to save and close the editor.\
-- By the way, the drawing and moving of buttons that you do outside the editor is saved automatically.",["y"]=43,["meta"]={},["label"]="demo",["w"]=100,["type"]="button",["h"]=20}},["code"]="alert(\"Buttons can have code to run when clicked.\",\
  \"Switch to button mode, then ...\",\
  \"drag the demo button to the top left.\")\
",["image_file"]="help-button2.png",["characters"]={},["name"]="help-button2",["type"]="card"}