return {["buttons"]={[1]={["x"]=4,["code"]="local button, card = ...",["y"]=7,["meta"]={["jump_to"]="character2"},["label"]="back",["w"]=100,["type"]="button",["h"]=20},[2]={["x"]=196,["code"]="local button, card = ...",["y"]=8,["meta"]={["jump_to"]="card1"},["label"]="next",["w"]=100,["type"]="button",["h"]=20}},["code"]="alert(\"Character code can do a lot more.\",\
  \"Drag this yellow thing to the top left.\")\
",["image_file"]="character3.png",["characters"]={["crystal"]={["x"]=46,["code"]="-- Let's get that character table again.\
local ch = ...\
\
-- But our character wants to react to key presses!\
ch.on_key_pressed = function(key)\
  -- We'll change its x and y fields when the arrows are pressed.\
  if(key == \"up\") then ch.y = ch.y - 2 end\
  if(key == \"down\") then ch.y = ch.y + 2 end\
  if(key == \"right\") then ch.x = ch.x + 2 end\
  if(key == \"left\") then ch.x = ch.x - 2 end\
end\
\
-- Other character functions you can write:\
-- * on_mouse_pressed(x, y)\
-- * on_mouse_moved(x, y, dx, dy)\
-- * on_mouse_released(x, y)\
",["costume"]=1,["y"]=54,["type"]="character",["name"]="crystal",["meta"]={}}},["name"]="character3",["type"]="card"}