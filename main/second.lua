return {["buttons"]={[1]={["x"]=194,["code"]="local button, card = ...",["y"]=4,["meta"]={["jump_to"]="help2"},["label"]="next",["w"]=100,["type"]="button",["h"]=19},[2]={["x"]=0,["code"]="lume.remove(card.current().buttons, ...)\
alert(unpack({[1]=\"Everything is made up of cards.\",[2]=\"Right-click to open the tools menu.\",[3]=\"Only the hand tool works to click buttons.\"}))",["y"]=120,["meta"]={["modal"]=true},["label"]="So... this is Cardinality. Click here.",["w"]=300,["type"]="button",["h"]=30,["bg"]={[1]=20,[2]=20,[3]=20,[4]=240}}},["code"]="alert(\"So... this is Cardinality. Click here.\",\
  \"Everything is made up of cards.\",\
  \"Right-click to open the tools menu.\",\
  \"Only the hand tool works to click buttons.\")\
",["image_file"]="second.png",["characters"]={},["name"]="second",["type"]="card"}