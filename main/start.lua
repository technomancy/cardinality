return {["buttons"]={[1]={["x"]=17,["code"]="card.new_stack()",["y"]=52,["meta"]={},["label"]="new stack",["w"]=100,["type"]="button",["h"]=20},[2]={["x"]=141,["code"]="local button, card = ...",["y"]=71,["meta"]={["jump_to"]="second"},["label"]="help",["w"]=100,["type"]="button",["h"]=20},[3]={["x"]=141,["code"]="quit()",["y"]=104,["meta"]={},["label"]="quit",["w"]=100,["type"]="button",["h"]=20},[4]={["x"]=17,["code"]="editor.read_line(\"Stack: \", function(name, cancel)\
  if(cancel) then return end\
  card.load(name) end, {completer=card.stack_completer, moused=true})",["y"]=86,["meta"]={},["label"]="load stack",["w"]=100,["type"]="button",["h"]=20},[5]={["x"]=17,["code"]="editor.read_line(\"Copy stack: \", function(name, cancel)\
    if(cancel) then return end\
    editor.read_line(\"New stack name: \", function(new_name, cancel)\
      card.copy_stack(name, new_name)\
    end)\
  end, \
  {completer=card.stack_completer, moused=true})",["y"]=119,["meta"]={},["label"]="copy stack",["w"]=100,["type"]="button",["h"]=20}},["code"]="",["image_file"]="start.png",["characters"]={},["name"]="start"}