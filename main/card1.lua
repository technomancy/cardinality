return {["buttons"]={[1]={["x"]=7,["code"]="local button, card = ...",["y"]=7,["meta"]={["jump_to"]="character3"},["label"]="back",["w"]=100,["type"]="button",["h"]=20},[2]={["x"]=195,["code"]="local button, card = ...",["y"]=7,["meta"]={["jump_to"]="card2"},["label"]="next",["w"]=100,["type"]="button",["h"]=20}},["code"]="-- As you may have guessed, every card has code just like characters.\
-- Everything here runs when you enter the card.\
\
alert(\"Finally, another few tools.\",\
  \"Right click and choose the {} one.\")\
\
-- But you can write handler functions here too:\
\
local c = ...\
\
c.on_key_pressed = function(key)\
  alert(\"You pressed \" .. key)\
end\
\
-- The mouse ones work here too.",["image_file"]="card1.png",["characters"]={},["name"]="card1",["type"]="card"}