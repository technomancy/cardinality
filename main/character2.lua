return {["buttons"]={[1]={["x"]=6.75,["code"]="local button, card = ...",["y"]=8,["meta"]={["jump_to"]="next"},["label"]="back",["w"]=100,["type"]="button",["h"]=20},[2]={["x"]=194,["code"]="local button, card = ...",["y"]=7,["meta"]={["jump_to"]="character3"},["label"]="next",["w"]=100,["type"]="button",["h"]=20}},["code"]="alert(\"Characters can have code like buttons can.\",\
  \"Drag this one to the top left.\")\
",["image_file"]="character2.png",["characters"]={["rock"]={["x"]=19,["code"]="-- Hoo boy! There's so many cool things you can do with characters.\
\
-- First let's get the character table and put it in a local variable.\
local ch = ...\
\
-- This will run whenever you enter the card that the character is in.\
print(\"This character is named \" .. ch.name)\
\
-- Oh, but where does the stuff you printed go?\
-- Press ctrl-enter (from anywhere) to toggle the console.\
\
-- The console is great because not only does it show output, it also lets you\
-- enter any code and run it immedaitely. This is great for testing things out\
-- quickly because you don't have to write a character's function and trigger\
-- it with the mouse, you can see what it does right away.\
\
-- Press escape to leave.",["costume"]=1,["y"]=36,["type"]="character",["name"]="rock",["meta"]={}}},["name"]="character2",["type"]="card"}