return {["buttons"]={[1]={["x"]=4,["code"]="local button, card = ...",["y"]=5,["meta"]={["jump_to"]="help-button2"},["label"]="back",["w"]=100,["type"]="button",["h"]=20},[2]={["x"]=196,["code"]="local button, card = ...",["y"]=4,["meta"]={["jump_to"]=false},["label"]="next",["w"]=100,["type"]="button",["h"]=20}},["code"]="alert(\"Not every button needs code though.\",\
  \"Some buttons just take you to a new card.\",\
  \"The 'next' button here does nothing.\",\
  \"Fix it by dragging it to the top right...\",\
  \"and typing 'next'. Then click it.\")\
",["image_file"]="help-button3.png",["characters"]={},["name"]="help-button3",["type"]="card"}