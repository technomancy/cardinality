local lume = require("lib.lume")

local ring_width = 32

-- menu target fields: color, selected_color, icon callback

local find_target = function(menu, x, y)
   local dx, dy = menu.active.x - x, menu.active.y - y
   local theta = math.atan2(dy, dx) + math.pi
   local r = math.sqrt(dx*dx+dy*dy)
   if(r <= ring_width * #menu) then
      local ring_num = math.ceil(r / ring_width)
      local ring = menu[math.max(1, ring_num)]
      local target_width = math.pi * 2 / #ring
      local target = ring[math.ceil(theta / target_width)]
      return target
   end
end

local radial = {
   mousemoved = function(menu, x, y)
      if(menu.active) then menu.over = find_target(menu, x, y) end
   end,

   mousepressed = function(menu, x, y)
      menu.active, menu.over = {x=x, y=y}, menu[1][1]
   end,

   mousereleased = function(menu, x, y)
      local target = find_target(menu, x, y)
      if(target and target.callback) then target.callback() end
      menu.active, menu.over = nil, nil
   end,

   draw = function(menu)
      if(menu.active) then
         local angle, target_width
         for i,ring in lume.ripairs(menu) do
            target_width, angle = math.pi * 2 / #ring, 0
            for _,target in ipairs(ring) do
               if(menu.over == target) then
                  love.graphics.setColor(target.selected_color or {255,255,255})
               else
                  love.graphics.setColor(target.color)
               end
               love.graphics.arc("fill", menu.active.x, menu.active.y,
                                 i*ring_width, angle, angle + target_width, 128)
               if(target.image_file and not target.image) then
                  target.image = love.graphics.newImage(target.image_file)
               end
               if(target.image) then
                  local x = menu.active.x + math.cos(angle + target_width/2) *
                     (i-0.5) * ring_width
                  local y = menu.active.y + math.sin(angle + target_width/2) *
                     (i-0.5) * ring_width
                  if(i == 1) then x, y = menu.active.x, menu.active.y end
                  love.graphics.draw(target.image, x, y, nil, nil, nil, 8, 8)
               end
               angle = angle + target_width
            end
         end
      end
   end,
}

return radial
