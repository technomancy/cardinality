local polywell = require("polywell")
local fs_for = require("fs")

love.keyreleased = polywell.keyreleased
love.keypressed = polywell.keypressed
love.textinput = polywell.textinput
love.wheelmoved = polywell.wheelmoved
love.mousepressed = polywell.mousepressed
love.mousereleased = polywell.mousereleased
love.mousemoved = polywell.mousemoved
love.mousefocus = polywell.mousefocus

love.load = function()
   local multiprint = function(x) print(x) polywell.print(x) end
   love.graphics.setFont(love.graphics.newFont("inconsolata.ttf", 14))
   love.keyboard.setTextInput(true)
   love.keyboard.setKeyRepeat(true)

   local init_file = os.getenv("HOME") .. "/.polywell/init.lua"
   polywell.fs = polywell.fs or fs_for(os.getenv("PWD"))
   if true then return dofile(init_file) end
   local ok, err = pcall(dofile, init_file)
   if(not ok) then
      multiprint(err)
      multiprint("Using default config.")
      local chunk = assert(love.filesystem.load("polywell/config/init.lua"))
      chunk("polywell_app")
   end
   polywell.fs = polywell.fs or fs_for(os.getenv("PWD"))
end
