# 0.2.0 / ???

* Rename event handler functions to match LÖVE naming.
* Write reference manual.
* Add `with_output_to` function to redirect `print` and `write`.
* Add mouse handlers.
* Allow modes to define their own wrap functions for custom undo.
* Add Emacs-style prefix maps for key bindings.

# 0.1.0 / 2016-09-04

* Initial release as a standalone project.
