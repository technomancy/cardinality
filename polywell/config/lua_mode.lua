-- Mode for -*- lua -*- files
local editor = require("polywell")

-- change color for comments:         red green blue
editor.set_colors({"lua", "comment"}, {10, 120, 10})

editor.define_mode("lua", "edit", {on_change = editor.colorize_lua,
                                   activate = editor.colorize_lua,})

editor.bind("lua", "ctrl-alt-r", editor.reload)
editor.bind("lua", "tab", editor.complete)