local editor = require("polywell")
local lume = require("polywell.lume")

require("polywell.config.edit")

editor.define_mode("shell", "edit") -- inherit bindings from edit

editor.bind("shell", "ctrl-a", editor.beginning_of_input)
editor.bind("shell", "home", editor.beginning_of_input)

editor.bind("shell", "alt-p", editor.history_prev)
editor.bind("shell", "alt-n", editor.history_next)
editor.bind("shell", "ctrl-up", editor.history_prev)
editor.bind("shell", "ctrl-down", editor.history_next)

local interpolate = function(s)
   local env = editor.get_props().ENV
   return (s:gsub('($%b{})', function(w) return env[w:sub(3, -2)] or w end))
end

editor.bind("shell", "return", function()
        -- if you're not on the last line, enter just bumps you down.
        if(editor.get_line_number() ~= editor.get_max_line()) then
           if(not jump_to_error()) then
              editor.end_of_buffer()
           end
        end

        local input = editor.get_input()
        editor.history_push(input)
        -- TODO: expand env vars in input
        editor.end_of_line()
        editor.newline()
        editor.no_mark()

        -- try to run the input
        local f = assert(io.popen(input))
        editor.with_output_to(editor.current_buffer_path(),
                              function() editor.print(f:read("*all")) end)
        f:close()
        editor.set_prompt(interpolate(env.PROMPT))
        editor.print_prompt()
end)

local function next_shell_buffer_name(n)
   local buffers = editor.buffer_names()
   local name = n and  "*shell:" .. n .. "*" or "*shell*"
   if(not lume.find(buffers, n)) then
      return name
   else
      return next_shell_buffer_name((n or 1)+1)
   end
end

local new_env = function()
   return { -- can't enumerate all existing ones, so inherit a whitelist
      HOME=os.getenv("HOME"),
      PWD=os.getenv("PWD"),
      PATH=os.getenv("PATH"),
      USER=os.getenv("USER"),
      PROMPT="${PWD} $ ",
      TERM="dumb",
   }
end

editor.start_shell = function()
   -- TODO: load rc file
   local env = new_env()
   editor.open(nil, next_shell_buffer_name(), {env=env})
   editor.set_prompt(interpolate(env.PROMPT))
   editor.print_prompt()
   editor.activate_mode("shell")
end
