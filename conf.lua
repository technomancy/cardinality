love.conf = function(t)
   t.version = "0.10.1"
   t.gammacorrect = true
   t.fullscreen = false
   t.title, t.identity = "Cardinality", "cardinality"
   t.modules.joystick, t.modules.physics = false, false
   t.modules.sound, t.modules.audio = false, false
end
