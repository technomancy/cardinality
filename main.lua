local polywell = require("polywell")
local fs_for = require("polywell.fs")
local card = require("card")

local traceback = function(...)
   -- requiring these in the top level causes a weird text align bug
   local utils = require("polywell.utils")
   local dialog = require("dialog")
   return utils.with_traceback(dialog.err, ...)
end

pp = function(x) print(require("lib.inspect")(x)) end

love.keyreleased = polywell.keyreleased
love.keypressed = polywell.keypressed
love.textinput = polywell.textinput
love.wheelmoved = polywell.wheelmoved
love.mousepressed = polywell.mousepressed
love.mousereleased = polywell.mousereleased
love.mousemoved = polywell.mousemoved
love.mousefocus = polywell.mousefocus
love.draw = polywell.draw
love.update = card.update

love.load = function(args)
   love.graphics.setFont(love.graphics.newFont("polywell/inconsolata.ttf", 14))
   love.keyboard.setTextInput(true)
   love.keyboard.setKeyRepeat(true)
   require("modes.play")
   require("modes.buttons")
   require("modes.button_edit")
   require("modes.character")
   require("modes.card_edit")
   require("modes.pencil")
   require("modes.line")
   require("modes.fill")
   require("modes.color")
   require("modes.dropper")
   require("modes.costume")
   require("modes.costume_pencil")
   require("modes.costume_dropper")
   require("modes.costume_fill")
   require("modes.costume_line")
   require("polywell.config.lua_mode")
   require("polywell.config.emacs_keys")

   local dir = args[2] or "main"
   polywell.fs = fs_for(dir)
   polywell.set_traceback(traceback)
   card.load(dir)
end
