local card = require("card")
local placeholder = "assets/placeholder.png"

return {
   new = function(name, x, y, code, meta)
      return {name=name, code=code or "local this_character, this_card = ...",
              meta=meta or {}, x=math.floor(x), y=math.floor(y),
              w=32, h=32, costume=1, type="character"}
   end,
   find = function(c, x, y)
      for _,ch in pairs(c.characters or {}) do
         if(x >= ch.x and x <= ch.x + (ch.w or 32) and
            y >= ch.y and y <= ch.y + (ch.h or 32)) then
            return ch
         end
      end
   end,
   draw = function(ch)
      ch.costume_images = ch.costume_images or {}
      if(not ch.costume_images[ch.costume]) then
         local dir = card.current().stack_meta.dir .. "/costumes/"
         local filename = dir..ch.name.."_"..ch.costume..".png"
         if(love.filesystem.isFile(filename)) then
            ch.costume_images[ch.costume] = love.graphics.newImage(filename)
         else
            ch.costume_images[ch.costume] = love.graphics.newImage(placeholder)
         end
         ch.costume_images[ch.costume]:setFilter('nearest', 'nearest')
      end
      love.graphics.draw(ch.costume_images[ch.costume], ch.x, ch.y)
   end,
   move = function(ch, x, y)
      ch.x, ch.y = ch.x + x, ch.y + y
      if(card.on_move(ch) == false) then
         ch.x, ch.y = ch.x - x, ch.y - y
      end
   end,
   move_to = function(ch, x, y)
      local old_x, old_y = ch.x, ch.y
      ch.x, ch.y = x, y
      if(card.on_move(ch) == false) then
         ch.x, ch.y = old_x, old_y
      end
   end,
   to_card = function(ch, card_name)
      local from, to = card.current(), card.find(card_name)
      from.characters[ch.name] = nil
      to.characters[ch.name] = ch
   end,
}
