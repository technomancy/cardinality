run:
	love .

LUA=*.lua modes/*.lua

check:
	luacheck --no-color --globals love pp -- ${LUA}

count:
	cloc ${LUA}

REL=".love-release/build/love-release.sh"
FLAGS=-a 'Phil Hagelberg' --description 'Hypertext authoring tool.' --love 0.10.1

mac:
	$(REL) $(FLAGS) -M

windows:
	$(REL) $(FLAGS) -W -W32

upload:
	rsync -rAv releases/ p:p/cardinality/
