local lume = require("lib.lume")
local polywell = require("polywell")
local inspect = require("lib.inspect")
local card = require("card")
local button = require("button")
local character = require("character")
local dialog = require("dialog")

-- Heads up! This module is stateful.

local sandbox = {
   assert = assert,
   error = error,
   pairs = pairs,
   ipairs = ipairs,
   next = next,
   pcall = pcall,
   xpcall = xpcall,
   select = select,
   tonumber = tonumber,
   tostring = tostring,
   type = type,
   unpack = unpack,
   pack = function(...) return {...} end,
   print = polywell.print,
   realprint = print,

   -- tables
   coroutine = lume.clone(coroutine),
   math = lume.clone(math),
   table = lume.clone(table),
   string = lume.clone(string),

   -- love
   graphics = lume.clone(love.graphics),
   keyboard = lume.clone(love.keyboard),
   mouse = lume.clone(love.mouse),

   -- custom
   lume = lume,
   editor = polywell,
   pp = function(x) polywell.print(inspect(x)) end,
   inspect = inspect,
   quit = love.event.quit,
   card = card,
   button = button,
   character = character,
   alert = dialog.alert,
}

local loadstring_sandboxed = function(code, where)
   local chunk, err = loadstring(code, where)
   if(chunk) then
      setfenv(chunk, sandbox)
      return chunk
   else
      return chunk, err
   end
end

sandbox.nosb = lume.fn(polywell.set_prop, "loadstring", loadstring)

return {
   load = function(x, ...)
      if(not x.code) then return end
      local chunk, err = loadstring_sandboxed(x.code, x.name or x.label)
      if(chunk) then
         return chunk(...)
      else
         polywell.print(err)
      end
   end,
   sandbox = sandbox,
   loadstring = loadstring_sandboxed,
   activate_sandbox = lume.fn(polywell.set_prop, "loadstring",
                              loadstring_sandboxed)
}
